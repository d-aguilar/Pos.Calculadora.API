﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pos.Calculadora.API.Business
{
    public static class Calculadora
    {
        public static int Multiplicar(int numero1, int numero2)
        {
            var resultado = numero1 * numero2;

            return resultado;
        }

        public static double RaizCubica(int numero)
        {
            var potencia = 1.0 / 3.0;
            var resultado = Math.Pow(numero, potencia);

            return resultado;
        }
        
        public static double Potencia(int numero, int fator)
        {
            var resultado = Math.Pow(numero, fator);
            return resultado;
        }

    }
}
