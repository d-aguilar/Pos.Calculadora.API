﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Pos.Calculadora.API.Controllers
{
    [Route("api/[controller]")]
    public class CalculoController : Controller
    {
        [HttpGet]
        [Route("api/Calculo/Multiplicar/{numero1}/{numero2}")]
        public int Multiplicar(int numero1, int numero2)
        {
            var resultado = Business.Calculadora.Multiplicar(numero1, numero2);

            return resultado;
        }

        [HttpGet]
        [Route("api/[controller]/RaizCubica/{numero}")]
        public double RaizCubica(int numero)
        {
            var resultado = Business.Calculadora.RaizCubica(numero);

            return resultado;
        }

        [HttpGet]
        [Route("api/[controller]/Potencia/{numero}/{fator}")]
        public double Potecia(int numero, int fator)
        {
            var resultado = Business.Calculadora.Potencia(numero, fator);

            return resultado;
        }


    }
}
